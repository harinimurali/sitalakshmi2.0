package com.school.sitalakshmi.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.school.sitalakshmi.Fragment.NumberVerification;
import com.school.sitalakshmi.R;

public class LoginActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        NumberVerification fragmentt = new NumberVerification();
        replaceFragment(fragmentt);
    }
    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_login, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onBackStackChanged() {

    }

    @Override
    public void onBackPressed() {
        Log.e("back_arrow pressed", "back_arrow pressed");

        FragmentManager manager = getSupportFragmentManager();

        Log.e("manager", "entrycount" + manager.getBackStackEntryCount());

        if (manager.getBackStackEntryCount() == 1) {
            finish();
        }
        super.onBackPressed();
    }
}

