package com.school.sitalakshmi.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.school.sitalakshmi.Activity.MainActivity;
import com.school.sitalakshmi.Helper.LanguageHelper;
import com.school.sitalakshmi.Helper.Permission;
import com.school.sitalakshmi.R;
import com.school.sitalakshmi.Utils.Constants;

/**
 * Created by harini on 9/17/2018.
 */

public class NewsFragment extends Fragment {
    WebView webView;
    public NewsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.webview, container, false);
        if (Permission.checknetwork(getActivity())) {
            final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "", "Please wait.. ", true);
            progDailog.setCancelable(false);
//terms_of_service_webview.getSettings().setJavaScriptEnabled(true);
            webView = (WebView) view.findViewById(R.id.webview);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progDailog.show();
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progDailog.dismiss();
                }
            });
            if (LanguageHelper.getLanguage(getActivity()).equals("en")) {
                webView.loadUrl(Constants.News_english_url);
            }else{
                webView.loadUrl(Constants.News_tamil_url);
            }
            webView.setOnKeyListener(new View.OnKeyListener(){

                public boolean onKey(View v, int keyCode, KeyEvent event) {
                  //  handler.sendEmptyMessage(1);
                    if (keyCode == KeyEvent.KEYCODE_BACK
                            && event.getAction() == MotionEvent.ACTION_UP
                            && webView.canGoBack()) {
                        handler.sendEmptyMessage(1);
                        return true;
                    }

                    return false;
                }

            });
        }
        return view;
    }
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:{
                    webViewGoBack();
                }break;
            }
        }
    };
    private void webViewGoBack(){
        webView.goBack();
    }
    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.news));
    }
}
